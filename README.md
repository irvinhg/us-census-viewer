
# US-Census-Viewer

# Building the app

## Dependencies

- polymer-cli
- bower
- mvn
- npm
- java 8

## Building the app

1 ) Get bower dependencies:

`bower install`

This should copy all of the Polymer dependencies.

2 ) Build static files

`npm run build`

It  will run a small npm script that will copy all of the files to a static folder.

3 ) Build the java sources and create a jar.

`mvn clean install`. Alternatively, you can use the Spring maven wrapper `mvnw clean install`

## Working with the Polymer app

If you want to modify the web application then you need to run the project in `dev` mode. You can go to `src/main/java/resources/webapp/us-census-viewer/` and run `polymer serve` which will create a small server to quickly test your frontend code. Don't forget to modify the `host` variable of the `us-census-viewer.html` element so that it points to the correct backend :).

## Building the Docker image

If you want to create a Docker image then you first need to build the Big Jar and then build the image.

1 ) Build with `mvnw clean install`

2 ) Copy the generated jar to `/docker/` and `cd /docker/`.

3 ) Build the image `sudo docker build -t uscensusviewer .` .

4 ) Run the image `sudo docker run -p 8082:8081 --name uscensus1 uscensusviewer`.
