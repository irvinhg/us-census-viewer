package com.irvinhg.demo.data;

import java.util.stream.Stream;

import javax.sql.DataSource;

import com.irvinhg.demo.FieldInformation;

public interface CensusDao {
	
	void setDataSource(DataSource dataSource);
	
	DataSource getDataSource();
	
	Stream<FieldInformation> getAveragesByField(String field);
	
	Stream<String> getFields();
	
}
