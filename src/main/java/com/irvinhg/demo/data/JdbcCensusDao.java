package com.irvinhg.demo.data;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.metadata.TableMetaDataContext;
import org.springframework.jdbc.support.DatabaseMetaDataCallback;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.stereotype.Repository;

import com.irvinhg.demo.DatabaseViewerApplication;
import com.irvinhg.demo.FieldInformation;

@Repository
public class JdbcCensusDao implements CensusDao {
	
	private static final Logger log = LoggerFactory.getLogger(DatabaseViewerApplication.class);
	
	// Old school method for reading table names - from : http://forum.springsource.org/showpost.php?p=5248&postcount=5
	private static class ReadTableNames implements DatabaseMetaDataCallback {

        @SuppressWarnings({ "rawtypes", "unchecked" })
		public Object processMetaData(DatabaseMetaData dbmd) throws SQLException {
            ResultSet rs = dbmd.getTables(dbmd.getUserName(), null, null, new String[]{"TABLE"});
            ArrayList l = new ArrayList();
            while (rs.next()) {
                l.add(rs.getString(3));
            }
            return l;
        }
    }
	
	private TableMetaDataContext context;
	private	JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	private String tableName;
	private List<String> columns;
	
	private List<String> getTableNames() {

		try {
			
			ReadTableNames getTableNames = new ReadTableNames();
			
			@SuppressWarnings("unchecked")
			ArrayList<String> tableNames = (ArrayList<String>) JdbcUtils.extractDatabaseMetaData(dataSource, getTableNames);
			
			return tableNames;
			
		} catch (MetaDataAccessException | ClassCastException e) {

			throw new RuntimeException(e);
		}
	}
	
	private List<String> getTableColumns(String tableName) {
		
		context.setTableName(tableName);
		context.processMetaData(dataSource, Collections.<String>emptyList(), new String[0]);

		return context.getTableColumns();
	}
	
	@Override
	public void setDataSource(DataSource dataSource) {
		
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.context = new TableMetaDataContext();
		
		List<String> tableNames = getTableNames();
		if (tableNames.size() != 1) {
			throw new RuntimeException("Only DBs with one table are supported.");
		}
		
		this.tableName = tableNames.get(0);
		if (this.tableName == null) {
			throw new RuntimeException("Could not read table name.");
		}
		
		this.columns = getTableColumns(this.tableName);
		if (this.columns.size() == 0) {
			throw new RuntimeException(String.join(" ", "Table", tableName, "does not contain any columns."));
		}
		
		// TODO: Should check that type is integer...
		if (!this.columns.contains("age")) {
			throw new RuntimeException(String.join(" ", "Table", tableName, "does not contain column 'age'."));
		}
	}

	@Override
	public Stream<FieldInformation> getAveragesByField(String field) {
		
		if (this.dataSource == null) {
			throw new RuntimeException("DAO has not been initialized.");
		}
		
		// Quick and dirty check for column...
		if (!this.columns.contains(field)) {
			throw new RuntimeException("Field '" + field + "' does not exist.");
		}
		
		final String QUERY = "SELECT \"${FIELD}\", ROUND(AVG(age)) as age, COUNT(\"${FIELD}\") as count FROM ${TABLE} WHERE age IS NOT NULL GROUP BY \"${FIELD}\" ORDER BY count DESC"
				.replaceAll("\\$\\{FIELD\\}", field)
				.replaceAll("\\$\\{TABLE\\}", tableName);
		
		log.info("Performing QUERY: " + QUERY);
		
		return jdbcTemplate.query(QUERY,
				(row, rowNum) -> { return new FieldInformation(row.getString(1), row.getInt(2), row.getLong(3)); })
				.stream();
	}

	@Override
	public Stream<String> getFields() {
		
		if (this.dataSource == null) {
			throw new RuntimeException("DAO has not been initialized.");
		}
		
		// Remove 'age' from fields
		return this.columns.stream().filter( (c) -> !c.equals("age") );
	}

	@Override
	public DataSource getDataSource() {
		return this.dataSource;
	}

}
