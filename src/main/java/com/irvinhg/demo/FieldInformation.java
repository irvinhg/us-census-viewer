/**
 * POJO for mapping rows and sending to client.
 */

package com.irvinhg.demo;

public class FieldInformation {
	
	private int value;
	private String label;
	private long count;
	
	public FieldInformation(String value, int age, long count) {
		super();
		this.value = age;
		this.label = value;
		this.count = count;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int age) {
		this.value = age;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String value) {
		this.label = value;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
	
	@Override
	public String toString() {
		
		return value + " , " + label + " , " + count;
	}
}
