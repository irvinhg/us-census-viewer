/**
 * Inspired by Spring example : https://github.com/spring-guides/gs-uploading-files/blob/master/complete/src/main/java/hello/storage/StorageService.java
 */

package com.irvinhg.demo.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {
	
    void init();

    void store(MultipartFile file);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(String filename);
    
    Path getRootLocation();

    void deleteAll();
}
