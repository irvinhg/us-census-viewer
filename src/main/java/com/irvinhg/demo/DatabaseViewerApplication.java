package com.irvinhg.demo;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.irvinhg.demo.storage.StorageProperties;
import com.irvinhg.demo.storage.StorageService;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class DatabaseViewerApplication  {
	
	private static final Logger log = LoggerFactory.getLogger(DatabaseViewerApplication.class);
	
	private static DataSource dataSource = null;
			
	public static void main(String[] args) {
		SpringApplication.run(DatabaseViewerApplication.class, args);
	}
	
	public static DataSource getSQLiteDataSource() {
		return dataSource;
	}
	
	public static void setSQLiteDataSource(String databaseFile) {
		
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.sqlite.JDBC");
        dataSourceBuilder.url("jdbc:sqlite:" + databaseFile);
        dataSource = dataSourceBuilder.build();
	}
	
    @Bean
    CommandLineRunner init(StorageService storageService) {
        return (args) -> {
            storageService.deleteAll();
            storageService.init();
        };
    }
}


