/**
 * Stateful Controller. Provides File Upload and setting DataSource API.
 */

package com.irvinhg.demo;

import java.io.File;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.irvinhg.demo.data.CensusDao;
import com.irvinhg.demo.storage.StorageService;

@Controller
@CrossOrigin(origins = "*")
public class DatabaseUploadController {
	
	@SuppressWarnings("unused")
	private static class Response {
		 
		private List<FieldInformation> averages;
		private String field;
		private int totalCount;
		private int clippedRows;
		
		public int getClippedRows() {
			return clippedRows;
		}
		public void setClippedRows(int clippedRows) {
			this.clippedRows = clippedRows;
		}
		public List<FieldInformation> getAverages() {
			return averages;
		}
		public void setAverages(List<FieldInformation> averages) {
			this.averages = averages;
		}
		public String getField() {
			return field;
		}
		public void setField(String field) {
			this.field = field;
		}
		public int getTotalCount() {
			return totalCount;
		}
		public void setTotalCount(int totalCount) {
			this.totalCount = totalCount;
		}
	}
	
	private static class Fields {
		
		private List<String> fields;

		public List<String> getFields() {
			return fields;
		}
		public void setFields(List<String> fields) {
			this.fields = fields;
		}
	}

    private final StorageService storageService;
    private final CensusDao dao;
    
    @Autowired
    public DatabaseUploadController(StorageService storageService, CensusDao dao) {
        this.storageService = storageService;
        this.dao = dao;
    }

    @GetMapping("/")
    public String landingPage() {
    	return "forward:/index.html";
    }
    
    @GetMapping("/api/list")
    public ResponseEntity<String> listUploadedFiles() {

    	Collection<String> files = storageService.loadAll()
    			.map(path -> path.getFileName().toString())
                .collect(Collectors.toList());
    	
    	ResponseEntity<String> response = new ResponseEntity<>(files.toString(), HttpStatus.OK);
    	
    	return response;
    }
    
    @GetMapping("/api/getaverage/{field}")
    public ResponseEntity<Response> getAverageByField(@PathVariable("field") String field) {

    	Response resp = new Response();
    	final int MAX_COUNT = 100;
    	List<FieldInformation> fieldInfo = dao.getAveragesByField(field).collect(Collectors.toList());
    	
    	// Set total number of rows
    	resp.setTotalCount(fieldInfo.size());
    	
    	// Get number of rows that were clipped
    	if (fieldInfo.size() >= MAX_COUNT) {
    		
    		List<FieldInformation> clippedRows = fieldInfo.subList(100, fieldInfo.size());
    		int count = 0;
    		for (FieldInformation fieldInformation : clippedRows) {
				count += fieldInformation.getCount();
			}
    		resp.setClippedRows(count);
    	}
    	
    	fieldInfo = fieldInfo.subList(0, Math.min(fieldInfo.size(), MAX_COUNT));
    	resp.setAverages(fieldInfo);
    	
    	return new ResponseEntity<>(resp, HttpStatus.OK);
    }
    
    @ExceptionHandler(FileUploadException.class)
    @PostMapping("/api/upload")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) throws Exception {
    	
    	dropFile();
    	storageService.store(file);
 		return new ResponseEntity<>("ok", HttpStatus.OK);
    }
    
    @PostMapping("/api/drop")
    public ResponseEntity<String> dropFile() throws Exception {
    	
    	// Recreate directory, we could just delete the file though..
    	storageService.deleteAll();
    	storageService.init();
    	return new ResponseEntity<>("ok", HttpStatus.OK);
    }
    
    @PostMapping("/api/setdatasource")
    public ResponseEntity<Fields> setDataSource() throws Exception {
        
    	Optional<Path> filePath = storageService.loadAll().findFirst();
    	if (!filePath.isPresent()) {
    		throw new Exception("No file uploaded yet.");
    	}

        // Set DataSource
        DatabaseViewerApplication.setSQLiteDataSource( storageService.getRootLocation().toString() + File.separator + filePath.get().toString() );
        dao.setDataSource(DatabaseViewerApplication.getSQLiteDataSource());
        storageService.deleteAll();
        
        List<String> fields = dao.getFields().map(String::toLowerCase).sorted().collect(Collectors.toList());
        Fields response = new Fields();
        response.setFields(fields);
        
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
